# Noise Project - REST API

REST API de Noise Project

## Prérequis

```
node
npm
mongodb
```

## Install

```
git clone git@gitlab.com:tjn.raj/noiseback.git
cd noiseBack
npm install
node index.js
```

## REST API

### Get list of Noises
=====

Request

`GET /api/noises`

`curl -i -H 'Accept: application/json' http://localhost:3000/api/noises`

Response

```
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 813
ETag: W/"32d-pnXOBGDXZTJ6/U3bG8GtTLeQrgY"
Date: Mon, 04 May 2020 23:05:23 GMT
Connection: keep-alive

[]
```

### Gett a Noise
=====

`GET /api/noises/:id`

`curl -i -H 'Accept: application/json' http://localhost:3000/api/noises/5eb00aca1f402e0b6ce89846`

Response 

```
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 218
ETag: W/"da-qGj0jc9UwcCF1VWJ59Oo/7XI0WY"
Date: Mon, 04 May 2020 23:16:59 GMT
Connection: keep-alive

[]
```

### Create a new Noise
=====

Request

`POST /api/noises`
  - email : mail
  - myphoto : jpg file
  - myaudio : audio file

### Create a new Video
=====

Request

`POST /api/videos/:id`
  - myvideo : video file

# Authors 

Narmaraj Thayanithy