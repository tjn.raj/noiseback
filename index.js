const express = require('express');
const cors = require('cors');
const app = express();
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser')
//var nodemailer = require('nodemailer');

app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));

app.use(fileUpload());
app.use(cors());

app.use(express.static('./noises'));

app.set('view engine', 'pug');

const mongoose = require('mongoose');
const Noise = require('./noise');

mongoose.connect('mongodb+srv://noise:noise@rajapi-nckah.mongodb.net/noise?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true})

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/api/noises', async (req, res) => {
  const noises = await Noise.find();
  await res.json(noises);
});

app.get('/api/noises/:id', async (req, res) => {
  const id = req.params.id;
  const noise = await Noise.findOne({_id : id});
  res.json(noise);
});

app.get('/api/latest', async (req, res) => {
  const noise = await Noise.find().sort({"createdAt": -1}).limit(1);
  noisePhoto = noise[0].photo;
  noiseAudio = noise[0].audio;

  noisePhoto = noisePhoto.replace('./noises/','http://3.92.78.170/');
  noiseAudio = noiseAudio.replace('./noises/', 'http://3.92.78.170/');

  noise[0].photo = noisePhoto;
  noise[0].audio = noiseAudio;
  await res.json(noise);
});

app.post('/api/noises',  (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }
  
  const email = req.body.email;
  let myphoto = req.files.myphoto;
  let myaudio = req.files.myaudio;

  if (!email || !myphoto  || !myaudio) {
    res.send('Il manque un argument')
    return
  };

  let photoUrl = `./noises/images/${myphoto.name}`;
  let audioUrl = `./noises/audios/${myaudio.name}`;

  myphoto.mv(photoUrl, function(err) {
    if (err)
      return res.status(500).send(err);
  });

  myaudio.mv(audioUrl, function(err) {
    if (err)
      return res.status(500).send(err);
  });

  const nouveau_noise = new Noise({
    email : email,
    photo : photoUrl,
    audio : audioUrl
  });
   
  nouveau_noise.save()
  res.json(nouveau_noise)
  return

});

app.post('/api/videos/:id', async (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  };

  const id = req.params.id;
  const noise = await Noise.findOne({_id : id});

  let myvideo = req.files.myvideo;
  let videoUrl = `./noises/videos/${myvideo.name}`;

  myvideo.mv(videoUrl, function(err) {
    if (err)
      return res.status(500).send(err);
  });

  noise.video = videoUrl;

  noise.save();
  res.json(noise);
  return;
});

app.get('/images/:id', async (req, res) => {
  const id = req.params.id;
  const noise = await Noise.findOne({_id : id});
  const myphoto = noise.photo;
  const file = myphoto;
  res.download(file);
});

app.get('/audios/:id', async (req, res) => {
  const id = req.params.id;
  const noise = await Noise.findOne({_id : id});
  const myaudio = noise.audio;
  const file = myaudio;
  res.download(file);
});

app.put('/api/noises/:id', async (req, res) => {
  const id = req.params.id;
  const noise = await Noise.findByIdAndUpdate(id,{$set:req.body}, function(err, result){
    if(err){
      res.send(err)
      return
    }
    res.send(result)
  });
});

app.get('/historique', async (req, res) => {
  const noises = await Noise.find({ video: { $regex: /w*[a-zA-Z]/ }}).sort({video: -1}).limit(10);

  const videos = noises.map(function(item) {
    videoUrl = item.video.replace('./noises', 'http://3.92.78.170/');
    return videoUrl
  });

  res.render('historique.pug', { title: 'Hey', message: 'Hello there!', videos: videos});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
