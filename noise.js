const mongoose = require('mongoose');

const schema = mongoose.Schema({
  email: { type: String, required: true},
  photo: { type: String, required: true},
  audio: { type: String, required: true},
  video: { type: String, default: ''},
  status: { type: String, default: 'pending'},
  createdAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('noise', schema);